package spedycja;

public class Osoba {

    private String nazwisko;
    private String imie;
    private String adres;

    public Osoba (String imie, String nazwisko, String adres){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
    }

    public String getImie() {return imie;}
    public String getNazwisko() {return nazwisko;}
    public String getAdres() {return adres;}
}
