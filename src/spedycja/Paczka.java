package spedycja;

import java.util.Scanner;

enum Status {przyjeta, doreczanie, doreczona};

public class Paczka {
    private Status stan = Status.przyjeta; // deklaracja z zainicjowaniem zmiennej
    private String nadawcaImie, nadawcaNazwisko, nadawcaAdres; // deklaracja zmiennych prywatnych
    private String odbiorcaImie, odbiorcaNazwisko, odbiorcaAdres; //deklaracja zmiennych prywatnych
    private String numer_przesyłki; //deklaracja zmiennej prywatnej
    private double waga; //deklaracja zmiennej prywatnej

    public String getAdres_nadawcy() {
        return nadawcaAdres;
    } // metoda publiczna pobiera adres nadawcy i go zwarca String

    public String getAdres_odbiorcy() {
        return odbiorcaAdres;
    } // metoda publiczna pobiera odres odbiorcy o go zwraca

    public double getWaga() {
        return waga;
    } // metoda publiczna zwracająca zmienna typu double

    public Status getStan() {
        return stan;
    } // mttoda zwracająca stan typu enum

    public String getnadawcaImie() {
        return nadawcaImie;
    } //

    public String getnadawcaNazwisko() {
        return nadawcaNazwisko;
    }

    public String getodbiorcaImie() {
        return odbiorcaImie;
    }

    public String getodbiorcaNazwisko() {
        return odbiorcaNazwisko;
    }

    public String getNumer_przesyłki() {
        return numer_przesyłki;
    }

    Paczka(String numer_przesyłki, double waga, String nadawcaImie, String nadawcaNazwisko, String nadawcaAdres,
           String odbiorcaImie, String odbiorcaNazwisko, String odbiorcaAdres)    //konstruktor
    {
        this.nadawcaImie = nadawcaImie;
        this.nadawcaNazwisko = nadawcaNazwisko;
        this.nadawcaAdres = nadawcaAdres;
        this.odbiorcaImie = odbiorcaImie;
        this.odbiorcaNazwisko = odbiorcaNazwisko;
        this.odbiorcaAdres = odbiorcaAdres;
        this.numer_przesyłki = numer_przesyłki;
        this.waga = waga;
    }

    public void zmienStatusPrzesylki(Status stPrzesylki) // metoda publiczna która zmienia status przesyłki
    {
        this.stan = stPrzesylki; // to znaczy że ta stPrzesylki odwołuje się do zmiennej Status(enum) Stan
    }

    public String podajStatus() //metoda wypisuje tekst w zależności od danej sytuacji
    {
        switch (stan) // odnosi się do zmiennej enum Status stan
        {
            case przyjeta:
                return "przyjęta"; // jeżeli stan = przyjęta to wydrukuj "przyjęta"
            case doreczanie:
                return "w trakcie doręczania";// jeżeli stan =w trakcie doręczenia  to wydrukuj "w trakcie doręczenia"
            case doreczona:
                return "doręczona"; // jeżeli stan = doręczona to wydrukuj "doręczona "
            default:
                return "zły status"; //jeżeli stan jest inny niż powyższe to zwróć "zły stan"
        }
    }


}
