package spedycja;

import java.util.ArrayList;

public class Kurier extends Osoba {

    public String przesylka;
    private ArrayList<Paczka> listaPaczek;

    public Kurier(ArrayList<Paczka> listaPaczek, String imie, String nazwisko) {
        super(imie, nazwisko, "");
        this.listaPaczek = listaPaczek;
        this.przesylka = "";
    }

    public int odbierzPrzesylke(String nrPrzesylki) {
        for (Paczka paczka : this.listaPaczek) {
            if (paczka.getNumer_przesyłki().equals(nrPrzesylki)) {
                if (paczka.getStan() == Status.przyjeta) {
                    paczka.zmienStatusPrzesylki(Status.doreczanie);
                    przesylka = nrPrzesylki;
                    return 0;
                } else if (paczka.getStan() == Status.doreczona) {
                    return -1;
                } else if (paczka.getStan() == Status.doreczanie) {
                    return -2;
                }
            }
        }
        return -3;
    }

    public int dostarczPrzesylke() {
        if (przesylka.equals("")) return -1;
        for (Paczka paczka : this.listaPaczek) {
            if (paczka.getNumer_przesyłki().equals(przesylka)) {
                paczka.zmienStatusPrzesylki(Status.doreczona);
                przesylka = "";
                return 0;
            }
        }
        return -2;
    }
}

