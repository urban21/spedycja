
package spedycja;

public class Nadawca extends Osoba {

    private String nrPrzesylki;

    Nadawca(String imie, String nazwisko, String adres) {
        super(imie, nazwisko, adres);
    }

    void setNrPrzesylki(String nrPrzesylki) {
        this.nrPrzesylki = nrPrzesylki;
    }

    String getNrPrzesylki() {
        return nrPrzesylki;
    }

}
