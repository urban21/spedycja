package spedycja;

import java.util.ArrayList;
import java.util.Date;


public class Spedytor extends Osoba {

    private ArrayList<Paczka> listaPaczek;

    public Spedytor(ArrayList<Paczka> listaPaczek, String imie, String nazwisko) { // konstruktor (spedytor ma dostęp do listy paczek)
        super(imie, nazwisko, ""); // oznacza wywołanie konstruktora klasy bazowej z podanymi w nawiasie argumentami
        this.listaPaczek = listaPaczek;
    }

    public String wprowadzPaczkeDoSystemu(String nadawcaImie, String nadawcaNazwisko, String nadawcaAdres,
                                          String odbiorcaImie, String odbiorcaNazwisko, String odbiorcaAdres) {

        if (this.listaPaczek == null)
            return ""; // this.listaPaczek == null - jeśl listaPaczek nie została jeszcze utworzona to  zwróć ""
        int d = (int) new Date().getTime();  // zmienna pseudolosowa, za pomocą pobranego z komputra czasu losuje liczbę
        Paczka paczka = new Paczka(String.valueOf(Math.abs(d)), 10, nadawcaImie, nadawcaNazwisko, nadawcaAdres, // typ_zmiennej.valueOf() - konwersja na typ_zmiennej
                odbiorcaImie, odbiorcaNazwisko, odbiorcaAdres);                              // Math.abs(d) - bez znaku
        this.listaPaczek.add(paczka);
        return paczka.getNumer_przesyłki();
    }

}
