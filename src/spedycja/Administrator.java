
package spedycja;

import java.util.ArrayList;


public class Administrator extends Osoba {
    private ArrayList<Kurier> listaKurierow;

    public Administrator(ArrayList<Kurier> listaKurierow, String imie, String nazwisko) {
        super(imie, nazwisko, "");
        this.listaKurierow = listaKurierow;
    }

    public void dodajKuriera(ArrayList<Paczka> listaPaczek, String imie, String nazwisko) {
        if (this.listaKurierow != null) {
            Kurier kurier = new Kurier(listaPaczek, imie, nazwisko);
            listaKurierow.add(kurier);

        }
    }

    public int usunKuriera(int index) {
        if (this.listaKurierow == null) return -3;
        if (listaKurierow.size() == 0) return -1;
        if (index == -1) return -2; // brak kurierów na liscie
        if (index < 0 || index > listaKurierow.size() - 1) return -3;
        Kurier kurier = listaKurierow.get(index);

        if (kurier.przesylka.equals("")) {
            listaKurierow.remove(index);
            return 0;
        } else return -4; // kurier posiada przesyłkę

    }

}
