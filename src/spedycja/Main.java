package spedycja;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static ArrayList<Paczka> listaPaczek = new ArrayList<Paczka>();    // static to samo co zmienna globalna w c++, static nie zmienia wartości jest statyczna
    public static ArrayList<Kurier> listaKurierow = new ArrayList<Kurier>();
    public static Administrator admin;
    public static Nadawca nadawca;
    public static Spedytor spedytor;


    public static void main(String[] args) {

        listaKurierow.add(new Kurier(listaPaczek, "Andrzej", "Wąsaty"));
        listaKurierow.add(new Kurier(listaPaczek, "Michał", "Brodaty"));
        admin = new Administrator(listaKurierow, "Dariusz", "Nowak");
        nadawca = new Nadawca("Jan", "Kowalski", "Akacjowa 10, 00-950 Warszawa");
        spedytor = new Spedytor(listaPaczek, "Olaf", "Olkowski");


        System.out.println(Kurier.class);




    }
}
